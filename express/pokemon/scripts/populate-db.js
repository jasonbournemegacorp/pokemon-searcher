var mongoose = require('mongoose');
var pokemon = require('./../pokemon-api.json');
var Pokemon = require('./../model/Pokemon');
var DB_CONNECTION = 'mongodb://localhost:27017/pokemon';

mongoose.connect(DB_CONNECTION);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function(){
	console.log('open db connection');
});

function populateDB() {
    // Run this on first startup
    for (each in pokemon){
      pokemon[each].name = pokemon[each].name.charAt(0).toUpperCase() + pokemon[each].name.slice(1)
    }
    Pokemon.insertMany(pokemon, function(err) {
        if (err) throw err
        console.log('Done');
	process.exit()
    });
  }

populateDB();
