const mongoose = require('mongoose');

var Schema = mongoose.Schema;

var pokemonSchema = new Schema({
	name: String,
	types: [String]
});

// TODO: For faster lookups create index
// pokemonSchema.index({name: 1, types: 1}, {unique: true});

pokemonSchema.methods.fuzzySearcher = function(searchString, cb){
	var reg = new RegExp(searchString, 'i')
	var jsonQuery = {$or: [{name:reg}, {types:reg}]};
    var jsonProjection = { name: 1, _id:0, types:1 };
	return this.model('Pokemon').find(jsonQuery, cb).select(jsonProjection);
};
pokemonSchema.methods.findAll = function(cb){
    var jsonProjection = { name: 1, _id:0, types:1 };
	return this.model('Pokemon').find(cb).select(jsonProjection);
};

var Pokemon = mongoose.model('Pokemon', pokemonSchema);

module.exports = Pokemon;
