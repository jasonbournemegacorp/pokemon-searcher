var express = require('express');
var router = express.Router();
var Pokemon = require('./../model/Pokemon');

/* GET home page. */
router.get('/api/pokemon', function(req, res, next) {
  Pokemon().findAll(function(err, pokemon){
    res.json(pokemon)
  })
});

router.get('/api/search/:query/:db_num', function(req, res, next){
  var query = req.params.query
  var client_num = req.params.db_num
  var db_num = 'dateOfDBw' // This will be determined by a db listener, we will use the date time
  if (client_num == db_num){
    // status = 401, body = 'you got it'
    res.status(304).send('you got it')
  }
  else{
    Pokemon().fuzzySearcher(query, function(err, pokemon){
      res.json(pokemon)
    })
  }

});

router.get('/api/dbchange/:query?', function(req, res, next){
  var client_nonce = req.params.query
  var db_nonce = 1
  res.json({'db_changed': client_nonce == db_nonce})
});

// set up for db listener
// db.collection('Pokemon').watch().
//     on('change', data => console.log(new Date(), data));

module.exports = router;
