# pokemon-project

> A Pokemon Searcher Project

## Setup

``` bash
# clone repo
git clone git@gitlab.com:jasonbournemegacorp/pokemon-searcher.git .

# Run backend server
cd express/pokemon && npm install
node scripts/populate-db.js
npm start

# Run frontend server
cd ../../frontend/pokemon-project/ && npm install
npm start
