import Vue from 'vue'
import Router from 'vue-router'
import PokemonSearcher from '@/components/PokemonSearcher'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'PokemonSearcher',
      component: PokemonSearcher
    }
  ]
})
