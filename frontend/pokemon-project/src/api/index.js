import axios from 'axios'
// axios is a promise based http-client

const API_URL = 'http://localhost:3000/api'

export function fetchPokemon () {
  return axios.get(`${API_URL}/pokemon/`)
}

export function queryPokemon (query, dbNum) {
  return axios.get(`${API_URL}/search/${query}/${dbNum}`)
}
